import 'package:flutter/material.dart';

import '../model/data/photo_list.dart';
import '../model/repository.dart';
import 'package:project_fazril/model/data/const.dart';
import 'package:project_fazril/model/data/image.dart';

class PhotoController extends ChangeNotifier {
  final List<Photo> _photos = [];
  bool _isLoading = false;
  final List<MyImage> _images = kImageList;
  final List<MyImage> _collection = [];
  final List<MyImage> _like = [];

  List<MyImage> get myCollection => _collection;
  List<MyImage> get myLike => _like;
  List<MyImage> get myImages => _images;

  List<Photo> get photos => _photos;
  bool get isLoading => _isLoading;

  setLoading(bool loading) {
    _isLoading = loading;
    notifyListeners();
  }

  fetchData({int page=1}) async {
    photos.clear();
    setLoading(true);
    final photoRepository = PhotoRepository();
    photos.addAll(await photoRepository.photos(page: page));
    setLoading(false);
    notifyListeners();
  }
  void add(MyImage image) {
    _collection.add(image);
    notifyListeners();
  }

  void remove(MyImage image) {
    _collection.remove(image);
    notifyListeners();
  }

  bool isMarked(MyImage image) {
    return _collection.contains(image);
  }
  
  void removeAll() {
    _collection.clear();
    notifyListeners();
  }

  void tambah(MyImage image){
    _like.add(image);
    notifyListeners();
  }
  bool isTanda(MyImage image) {
    return _like.contains(image);
  }
  void hapus(MyImage myImage){
    _like.clear();
    notifyListeners();
  }
}
