import 'package:project_fazril/model/data/image.dart';

List<MyImage> kImageList = [
  MyImage(
    userName: 'Jacob Black',
    avatarUrl: 'https://source.unsplash.com/random/100x100?men+portrait1',
    imageUrl: 'https://source.unsplash.com/random/400x500?minimalist+1',
    isMarked: false,
    isTanda:false,
  ),
  MyImage(
    userName: 'Goeorge White',
    avatarUrl: 'https://source.unsplash.com/random/100x100?men+portrait2',
    imageUrl: 'https://source.unsplash.com/random/400x500?minimalist+2',
    isMarked: false,
    isTanda:false,
  ),
  MyImage(
    userName: 'Anthony Grey',
    avatarUrl: 'https://source.unsplash.com/random/100x100?men+portrait3',
    imageUrl: 'https://source.unsplash.com/random/400x500?minimalist+3',
    isMarked: false,
    isTanda:false,
  ),
  MyImage(
    userName: 'Leo Teal',
    avatarUrl: 'https://source.unsplash.com/random/100x100?men+portrait4',
    imageUrl: 'https://source.unsplash.com/random/400x500?minimalist+4',
    isMarked: false,
    isTanda:false,
  ),
  MyImage(
    userName: 'John Cena',
    avatarUrl: 'https://source.unsplash.com/random/100x100?men+portrait5',
    imageUrl: 'https://source.unsplash.com/random/400x500?minimalist+5',
    isMarked: false,
    isTanda:false,
  ),
];
