import 'dart:html';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../controller/photo_controller.dart';
import '../model/data/photo_list.dart';
import 'package:project_fazril/model/image/image_list.dart';
import 'package:project_fazril/view/profile_widget.dart';
import 'photo_card.dart';

class HomePage extends StatefulWidget {
  
  const HomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentIndex = 0;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      final controller = Provider.of<PhotoController>(
        context,
        listen: false,
      );
      controller.fetchData();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => PhotoController(),
      child: DefaultTabController(
        animationDuration: Duration.zero,
        length: 3,
        child: Scaffold(
            appBar: currentIndex == 2 ? null : appBar(),
            body: body(),
            bottomNavigationBar: bottomTabBar()),
      ),
    );
  }
}
Container bottomTabBar() {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.only(bottom: 30, top: 6),
      child: TabBar(
        tabs: [
          tabItem(Icons.home),
          tabItem(Icons.bookmark),
          tabItem(Icons.person),
        ],
        labelColor: Colors.black,
        unselectedLabelColor: Colors.grey,
        indicatorColor: Colors.white,
      ),
    );
  }

  Tab tabItem(IconData icon) {
    return Tab(icon: Icon(icon, size: 30));
  }

  AppBar appBar() {
    return AppBar(
      toolbarHeight: kToolbarHeight,
      foregroundColor: Colors.black87,
      backgroundColor: Colors.white,
      elevation: 0,
      title: TextFormField(
        textAlignVertical: TextAlignVertical.center,
        decoration: InputDecoration(
            contentPadding: kTabLabelPadding,
            border: const OutlineInputBorder(
              borderSide: BorderSide.none,
              borderRadius: BorderRadius.all(Radius.circular(
                kRadialReactionRadius,
              )),
            ),
            fillColor: Colors.grey.withOpacity(0.15),
            filled: true,
            hintText: 'Search',
            hintStyle: const TextStyle(color: Colors.black26),
            prefixIcon: const Icon(
              Icons.search_rounded,
              color: Colors.black26,
              size: 22,
            )),
      ),
    );
    
  }
    TabBarView body() {
    return const TabBarView(
      physics: NeverScrollableScrollPhysics(),
      children: [
        ImageListWidget(),
        ImageListWidget(isCollection: true),
        ProfileWidget()
      ],
    );
  }




